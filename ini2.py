#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Variables and Some Arithmetic

http://rosalind.info/problems/ini2/
"""

import sys


DEBUG = False


def main():
    a = 980
    b = 949

    print(a**2 + b**2)


if __name__ == "__main__":
    main()
