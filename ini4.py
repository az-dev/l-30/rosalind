#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Conditions and Loops

http://rosalind.info/problems/ini4/
"""

import sys


DEBUG = False


def main():
    a = 4073
    b = 8828
    sum = 0

    for i in range(a, b):
        if i % 2 == 1:
            sum += i

    print(sum)


if __name__ == "__main__":
    main()
