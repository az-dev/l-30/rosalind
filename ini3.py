#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Strings and Lists

http://rosalind.info/problems/ini3/
"""

import sys


DEBUG = False


def main():
    s = 'W7C4O227xTringaNjopWgaX9czEhG0TFAJmeermaniNACGVMgxU0JHQcemFhteH6OM79sWpPJ433A6pOuNgCxxzNuJAEJQEyZ63SIUCOYSUt7ALhQQJqiomVYXA37g9l9c6kzKv0r4bCacBvNiZ9xyK10QhAnWYxumGsC4f48Bb5e2kU6IWigijflV2.'
    a = 9
    b = 14
    c = 34
    d = 41

    print(s[a:b+1], s[c:d+1])


if __name__ == "__main__":
    main()
